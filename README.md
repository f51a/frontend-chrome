# frontend-chrome

To test this chrome extension locally, go to chrome | more | extensions | enable developer mode | load unpacked extension | select this folder

# Useful links

[Debug](https://developer.chrome.com/docs/extensions/mv3/tut_debugging/)

[Become a verified published](https://developer.chrome.com/docs/webstore/cws-dashboard-listing/#displaying-your-verified-publisher-status)

[Publish](https://developer.chrome.com/docs/webstore/publish)
